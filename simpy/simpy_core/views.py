from django.shortcuts import render, HttpResponse
from .models import *
from django.template import Template, Context
# Create your views here.


def page_view(request, slug):
    pages = BasePageModel.objects.filter(slug=slug)
    if pages.count() == 0:
        page = BasePageModel.objects.filter(slug="not_found")[0]
    else:
        page = pages[0]

    page_template = BaseTemplateModel.objects.get(name=page.template)
    # print(page_template.code)
    template = Template(page_template.html)
    context = Context({
        "page": page
    })
    print(context)
    response = template.render(context)
    print(response)
    return HttpResponse(response)