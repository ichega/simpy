from django.contrib import admin
from .models import *
# Register your models here.


admin.site.register(BaseTemplateModel)
admin.site.register(BasePageModel)
admin.site.register(BaseSnippetModel)
admin.site.register(BaseBlockModel)