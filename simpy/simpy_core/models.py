from django.db import models


# Create your models here.


class BaseTemplateModel(models.Model):
    name = models.CharField(max_length=50, unique=True)
    html = models.TextField(verbose_name="HTML", max_length=20000, blank=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Шаблон"
        verbose_name_plural = "Шаблоны"


class BasePageModel(models.Model):
    title = models.CharField(max_length=80, default="Title")
    slug = models.SlugField(max_length=80, unique=True)
    template = models.ForeignKey(
        BaseTemplateModel,
        on_delete=models.CASCADE,
        related_name="+",
        blank=True,
    )
    html = models.TextField(verbose_name="Body", max_length=20000, blank=True, null=True)

    def __str__(self):
        return str(self.title) + f" /{str(self.slug)}/"

    class Meta:
        verbose_name = "Страница"
        verbose_name_plural = "Страницы"

class BaseBlockModel(models.Model):
    name = models.CharField(max_length=80)
    html = models.TextField(max_length=20000, blank=True)
    page = models.ForeignKey(BasePageModel, on_delete=models.CASCADE)



class BaseSnippetModel(models.Model):
    title = models.CharField(max_length=80)

    page = models.ForeignKey(BasePageModel, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name = "Фрагмент"
        verbose_name_plural = "Фрагменты"




