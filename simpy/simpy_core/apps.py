from django.apps import AppConfig


class SimpyCoreConfig(AppConfig):
    name = 'simpy_core'
